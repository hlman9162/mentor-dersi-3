Task 1:
Create a table with constraints : Create a table named 'Project_Details'
with columns 'Project_ID' (number type),
             'Project_Name' (varchar type),
             'Start_Date' (date type),
             'End_Date' (date type) and 
             'Manager_ID' (number type).


Task 2:
Insert a table  (Project_Details):
      (1,"Project1","20.05.2024","27.05.2024",10);
      (2,"Project2","10.05.2024","23.05.2024",20);
      (3,"Project3","15.05.2024","24.05.2024",30);
      (4,"Project4","16.05.2024","25.05.2024",40);
      (5,"Project5","07.05.2024","27.05.2024",50);

Task 3:
Alter table: Add a column 'Project_Status' of varchar type to the 'Project_Details' table.

Task 4:
Update operation: Update the 'Project_Details' table to set 'Project_Status' as 'Completed' for all projects whose 'End_Date' is before today's date.

Task 5:
Subquery: Write a query to find employees who earn more than the average salary.

Task 6:
Select with group by: Write a SELECT statement to get the job_id and the number of employees working in each job_id from the 'Employees' table.

Task 7:
DML Operation (UPDATE with WHERE)
Write a SQL statement to update the department id to 20 for all employees whose last name is 'King'

Task 8:
SELECT with DISTINCT clause
Write a SQL statement to select all distinct job titles from the 'employees' table.

Task 9:
SELECT with GROUP BY clause
Write a SQL statement to find the total salary for each job title.

Task 10:
SELECT with FETCH FIRST clause
Write a SQL statement to select the top 5 highest earning employees.

Task 11:
SELECT with WHERE and LIKE clause
Write a SQL statement to find all employees whose first name starts with 'A'.

Task 12:
SELECT with WHERE and IN clause
Write a SQL statement to find all employees who work in department 10, 20 or 30

Task 13:
SELECT with Aggregate Function and HAVING clause
Write a SQL statement to find the job titles with more than 5 employees.

Task 14:
SELECT with Aggregate Function and GROUP BY clause
Write a SQL statement to find the average salary for each job title.

Task 15:
SELECT with WHERE and NULL clause
Write a SQL statement to find all employees with no commission_pct.

Task 16:
--CHARACTER funksiyalarina aid bir numune yaz
--(UPPER,LOWER,INITCAP,LENGTH,SUBSTR,TRIM)

Task 17:
--NUMBER
--(CEIL,FLOOR,ROUND,TRUNC)

Task 18:
--DATE
--(SYSDATE,MONTHS_BETWEEN,ROUND,TRUNC)
