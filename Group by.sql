SELECT column_name(s),
       aggregate_function(column_name)
FROM table_name
WHERE condition
GROUP BY column_name(s);


COUNT(*): Counts the number of rows in each group.
SUM(column_name): Calculates the sum of a specific column for each group.
AVG(column_name): Calculates the average of a specific column for each group.
MIN(column_name): Returns the minimum value of a specific column for each group.
MAX(column_name):Returns the maximum value of a specific column for each group.

